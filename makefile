testtp4: main.o TP4.o 
	gcc -o testtp4 main.o TP4.o

main.o: main.c TP4.h
	gcc -c main.c

TP4.o: TP4.c TP4.h
	gcc -c TP4.c
					
clean:
	rm -f testtp4 *.o